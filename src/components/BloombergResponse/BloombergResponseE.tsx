import { AxiosError } from "axios";

interface BloombergResponseE_Props {
    data: AxiosError
}
export function BloombergResponseE(props: BloombergResponseE_Props) {
    console.log('BloombergResponseE()', props);
    return (<>
            <p><strong>message: </strong>{props.data.message} </p>
            <p><strong>responseText: </strong>{props.data.request.responseText} </p>
            <p><strong>status:</strong> {props.data.request?.status} {props.data.request?.statusText} </p>
    </>);    
}