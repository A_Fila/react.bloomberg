import { BloomResp } from "../BloombergLoad/Bloomberg";

interface BloombergResponseS_Props {
    data: BloomResp
}
export function BloombergResponseS(props: BloombergResponseS_Props) {
    //console.log('BloombergResponseS', props.)
    if (props.data !== null && props.data.fieldDataCollection == null) {
        return (
            <div className="bloombergResponseE">Error: unexpected response type: {JSON.stringify(props.data)}</div>
        );
    }
        
    return (<>
        <p>date: {props.data.quotDate}</p>
        <p>time: {props.data.quotTime}</p>
        <p>notification: {props.data.notification}</p>
            {/*BloombergResponseS.tsx:8 Warning: Each child in a list should have a unique "key" prop.*/}
            {props.data?.fieldDataCollection.map((r) =>
                <p key={r.id}  ><strong>{r.id}</strong> = <strong>{r.rate}</strong> </p>)
            }
    </>);    
}