import { AxiosError } from "axios";
import { BloomResp } from "../BloombergLoad/Bloomberg";
import { BloombergResponseS } from "./BloombergResponseS";
import { BloombergResponseE } from "./BloombergResponseE";
import { BloombergFrame } from "./BloombergFrame";

interface BloombergPesponseProps {
    isLoading: boolean,
    quots: BloomResp | null,
    err: AxiosError | null
}

export function BloombergResponse(props: BloombergPesponseProps) {

    // Loading...
    if (props.isLoading)
        return <h1>загрузка...</h1>
    else
        // Error
        if (props.err !== null)
            return <BloombergFrame className="bloombergResponseE">
                <BloombergResponseE data={props.err} />
            </BloombergFrame>
        else
            // Susscess
            if (props.quots !== null)
                return <BloombergFrame className="bloombergResponseS">
                    <BloombergResponseS data={props.quots} />
                </BloombergFrame>
            // Empty
            else return null;
}