﻿import { Component, PropsWithChildren, ReactNode } from 'react';
import { format } from 'date-fns';

interface BloombergFrameProps  {
    className: string
}

export function BloombergFrame(props: PropsWithChildren<BloombergFrameProps>) {
    return <div className={props.className}>
                <center><h2>Ответ сервиса</h2></center>
                <br/>
                {props.children}
                <p className='bloombergTimeStamp'>updated: {format(new Date(), 'yyyy-MM-dd HH:mm:ss')}</p>
           </div>
}

//interface Ramka1Props {
//    children: ReactNode;
//}
//export class Ramka1 extends Component<PropsWithChildren> {
//    render(){
//        return <div className="ramka">
//            <span>----Ramka 1------</span>
//        <br/>
//            {this.props.children}
//            <span>----Ramka 1 end------</span>
//            </div>


//    }
//}