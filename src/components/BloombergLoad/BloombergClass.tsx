//******************************************************************
//   CLASS  component
//*******************************************************************
import { format, subDays } from "date-fns";
import './Bloomberg.css'
import React, { BaseSyntheticEvent, useRef, useState } from "react";
import axios, { AxiosError} from "axios";
import { BloombergResponse } from "../BloombergResponse/BloombergResponse";
import { BloombergTimeList } from "./BloombergTimeList";
import { StringFormat } from "../../Util/StringFormat";
import { BloombergCurrencyList } from "./BloombergCurrencyList";

//CORS proxy server to bypass same-origin policy related to performing standard AJAX requests to 3rd party services.
//const CORS_PROXY = 'https://api.codetabs.com/v1/proxy?quest=';// 'https://cors-anywhere.herokuapp.com/'
//const CORS_PROXY = 'https://cors-anywhere.herokuapp.com/'

               //'https://www.bloomberg.com/markets/api/comparison/data-bfix?currencies=EURUSD&dateTime=2024-02-12T13:00:00Z';
const BASE_URL = 'https://www.bloomberg.com/markets/api/comparison/data-bfix?currencies={0}&dateTime={1}T{2}:00Z';

//https://www.bloomberg.com/markets/currencies/fx-fixings
//https://catfact.ninja/fact';

export interface BloomResp {
    notification: string;
    fieldDataCollection: { id: string, rate: number }[],
    quotDate?: string,
    quotTime?: string
}

interface BloombergState {
    err: AxiosError | null,
    data: BloomResp | null,
    isLoading: boolean,
    url: string
    //date: Date
}

interface BloombergProps {
    initDate: Date
    proxy: string
}

export class BloombergClass extends React.Component<BloombergProps, BloombergState> {
    refProxy = React.createRef<HTMLInputElement>();
    refDate = React.createRef<HTMLInputElement>();
    refUrl = React.createRef<HTMLInputElement>();

    time: string = '16:00|Europe/London';
    currency: string = 'EURRUB';

    constructor(props: BloombergProps) {
        super(props);    
          
        this.state = {
            err: null,
            isLoading: false,
            data: null,
            url: this.generateUrl()
            //date: props.initDate
        }
    
        console.log('ctor', this.state.isLoading);
    }
    componentDidMount(): void{
        console.log('componentDidMount');
    }
    componentWillUnmount(): void {
        console.log('componentWillUnmount');
    }

    onQuoteDateChanged = (e: any) => {
        //this.setState({ date: e.target.value })
        console.log('onQuoteDateChanged()');
        this.setState({ url: this.generateUrl() })//this.state.date+''
    }

    onLoad = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        const strUrl = '' + this.refProxy.current?.value + this.refUrl.current?.value;// 'https://www.bloomberg.com/markets/api/comparison/data-bfix?currencies=EURUSD&dateTime=2024-02-12T13:00:00Z'
        this.setState({ isLoading: true });

        console.log('onLoadClick()', strUrl);//strUrl
        axios.get<BloomResp>(strUrl)// this.state.url)
            .then(res => {
                console.log(format(new Date(), 'HH:mm:ss.SSS') + ' AXIOS_Resp:', res);
                //console.log('AXIOS_Resp: typeof(res.data)', typeof(res.data));
                if (typeof (res.data) === 'object') {
                    res.data.quotDate = this.refDate.current?.value
                    res.data.quotTime = this.time
                }
                //setData(res.data);
                this.setState({
                    err: null,
                    isLoading: false,
                    data: res.data,
                    //url: strUrl
                });
            })
            .catch(err => {
                //setErr(err)
                console.log(format(new Date(), 'HH:mm:ss.SSS') + ' AXIOS_Err: ', err);
                this.setState({
                    err: err,
                    isLoading: false,
                    data: null,
                    //url: strUrl
                });
            }).finally(() => {
                //setisLoading(false);
                console.log('AXIOS_Resp_finally');
            });         
    }//onLoad

    setTime = (time: string) => {
        console.log('setTime', time)
        this.time = time;
        this.refreshUrl();
    }   

    setCurrency = (currency: string) => {
        console.log('setCurrency', currency);
        this.currency = currency;
        this.refreshUrl();
    }  

    generateUrl = () => {
        const strDate: string = this.refDate.current?.value || format(this.props.initDate, 'yyyy-MM-dd');
        const url = StringFormat(BASE_URL, this.currency, strDate, this.time.substring(0, 5)); //'T'

       // console.log(format(new Date(), 'HH  :mm:ss.SSS') + ' generateUrl(): refTime=', this.refTime.current)

        return url; //2024-02-09T13:00:00Z
    }

    refreshUrl = () => {
        console.log('refreshUrl(): url=', this.generateUrl())
        this.setState({ url: this.generateUrl() });
    }

    render() {
       
        return (
            <>  <h1 style={{ color: 'red' }}>Поскольку сайт bloomberg.com не разрешает CORS-запросы, для вызова API используется PROXY-сайт

                <a href={this.props.proxy} target="_blank"> {this.props.proxy}</a>
                <br />
                <p>***********************************************************************************</p>
                <em>для временной разблокировки CORS Anywhere proxy требуется посещение страницы...(((</em>
                <p>***********************************************************************************</p>
            </h1>
                <br />
                <div className="bloombergInput">
                    <h1>Class component</h1>
                    <label htmlFor="proxyUrl">использовать PROXY: </label>
                    <input id="proxyUrl" type="text" defaultValue={this.props.proxy} ref={this.refProxy} />
                    <br /><br />

                    <label htmlFor="quoteDate">дата котировки: </label>
                    <input id="quoteDate" type="date" defaultValue={format(this.props.initDate, 'yyyy-MM-dd')}
                        onChange={this.onQuoteDateChanged}
                        ref={this.refDate}                    />

                    <BloombergTimeList default={this.time} onChange={this.setTime}  />
                    <BloombergCurrencyList default={this.currency} onChange={this.setCurrency} />

                    <br /> 

                    <label htmlFor="quoteUrl">адрес API: </label>
                    <input id="quoteUrl" type="text" placeholder="введите URL с любым адресом API, возвращающим JSON (например, https://catfact.ninja/fact)"
                        defaultValue={this.state.url}
                        key={this.state.url}   
                        ref={this.refUrl}
                    />
                    <br />
 

                {/* Button */}
                    <button onClick={this.onLoad}>Загрузить</button>

                </div>

                {<BloombergResponse quots={this.state.data} err={this.state.err} isLoading={this.state.isLoading} />}

            </>);
    }
}