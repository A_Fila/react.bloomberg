interface BloombergCurrencyListProps {
    default: string;
    onChange(v: string): void;
}

export function BloombergCurrencyList(props: BloombergCurrencyListProps) {
    const onChange = (e: React.ChangeEvent<HTMLSelectElement>) => { //SyntheticEvent<HTMLSelectElement>
        //console.log('BloombergCurrencyList.OnChange(): ', e.target.value)
        props.onChange(e.target.value)
    }
    return <span className="bloombergCurrency">

                <label htmlFor="currencyList">валюта: </label>

                <select id="currencyList" defaultValue={props.default} onChange={onChange}>            
                    <option value="EURRUB">EUR-RUB</option>
                    <option value="EURUSD">EUR-USD</option>
                    <option value="USDCNH">USD-CNH</option>           
        </select>

            </span>;
}
