interface BloombergTimeListProps {
    default: string;
    onChange(v:string): void;
}
export function BloombergTimeList(props: BloombergTimeListProps) {

    const onChange = (e: React.ChangeEvent<HTMLSelectElement>) => { //SyntheticEvent<HTMLSelectElement>
        //console.log('BloombergTimeList.OnChange(): ', e.target.value)
        props.onChange(e.target.value)
    }

    return <span className="bloombergTime">

        <label htmlFor="timesList">время: </label>

        <select id="timesList" defaultValue={ props.default} onChange={onChange }>
            <option value="09:30|Asia/Shanghai">Beijing 9:30 AM</option>
            <option value="11:00|Asia/Singapore">Singapore / Kuala Lumpur 11:00 AM</option>
            <option value="11:30|Asia/Manila">Manila 11:30 AM</option>
            <option value="11:00|Asia/Jakarta">Jakarta / Bangkok 11:00 AM</option>
            <option value="14:00|Asia/Seoul">Seoul 2:00 PM</option>
            <option value="15:00|Asia/Tokyo">Tokyo 3:00 PM</option>
            <option value="12:00|Asia/Kolkata">Mumbai 12:00 PM</option>
            <option value="16:00|Europe/London">London 4:00 PM</option>
            <option value="14:00|Europe/Berlin">Frankfurt 2:00 PM</option>
            <option value="12:30|America/New_York">New York 12:30 PM</option>
            <option value="17:00|America/New_York">New York 5:00 PM</option>
            <option value="12:00|America/Toronto">Ottawa 12:00 PM</option>
        </select>

    </span>;
}