import { format, subDays } from "date-fns";
import './Bloomberg.css'
import { BaseSyntheticEvent, useRef, useState } from "react";
import axios, { AxiosError} from "axios";
import { BloombergResponse } from "../BloombergResponse/BloombergResponse";
import { BloombergTimeList } from "./BloombergTimeList";
import { StringFormat } from "../../Util/StringFormat";
import { BloombergCurrencyList } from "./BloombergCurrencyList";

                //https://www.bloomberg.com/markets/currencies/fx-fixings
               //'https://www.bloomberg.com/markets/api/comparison/data-bfix?currencies=EURUSD&dateTime=2024-02-12T13:00:00Z';
const BASE_URL = 'https://www.bloomberg.com/markets/api/comparison/data-bfix?currencies={0}&dateTime={1}T{2}:00Z';

export interface BloomResp {
    notification: string;
    fieldDataCollection: { id: string, rate: number }[],
    quotDate?: string,
    quotTime?: string
}

interface BloombergProps {
    initDate: Date
    proxy: string
}

export const Bloomberg = (props: BloombergProps) => {
    
    //const initDate = getInitDate();  
    
    const [err,  setErr]  = useState<null | AxiosError>(null);
    const [data, setData] = useState<null | BloomResp>(null); 
    const [isLoading, setisLoading] = useState<boolean>(false);

    const refProxy = useRef<HTMLInputElement>(null);

    const refDate = useRef<HTMLInputElement>(null);
    const refCur  = useRef<string>("EURRUB");//EURRUB,EURUSD
    const refTime = useRef<string>("16:00|Europe/London");

    const refUrl = useRef<HTMLInputElement>(null);
    const [url, setUrl] = useState<string>(generateUrl());

    function generateUrl(): string {
        const strDate: string = refDate.current?.value || format(props.initDate, 'yyyy-MM-dd');
        const url = StringFormat(BASE_URL, refCur.current, strDate, refTime.current.substring(0, 5)); //'T'

        //console.log(format(new Date(), 'HH:mm:ss.SSS') + ' generateUrl(): ', url)

        return url; //2024-02-09T13:00:00Z
    }

    function refreshUrl() {
        console.log('refreshUrl(): refCur.current=', refTime)        
        setUrl(generateUrl());
    }

    //Button Load handler
    const onLoad = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => { //BaseSyntheticEvent
        console.log('onLoad()', e);
        //console.log('onLoad(): refUrl=', refUrl.current); //refUrl=', refUrl.current

        //clear state        
        setisLoading(true);
        setErr(null);  

        console.log('onLoadClick(): refProxy=', refProxy.current?.value);// url);
        const strUrl: string = '' + refProxy.current?.value + refUrl.current?.value;// || ''; CORS_PROXY
        //const strUrl: string = refUrl.current?.value ?? '';

        axios.get<BloomResp>(strUrl)// url)
            .then(res => {
                //console.log(format(new Date(), 'HH:mm:ss.SSS') + ' AXIOS_Resp:', res);
                //console.log('AXIOS_Resp: typeof(res.data)', typeof(res.data));
                if (typeof (res.data) === 'object') {
                    res.data.quotDate = refDate.current?.value
                    res.data.quotTime = refTime.current
                }             
                setData(res.data);
            })
            .catch(err => {
                setErr(err)
                //console.log(format(new Date(), 'HH:mm:ss.SSS') + ' AXIOS_Err: ', err);
            }).finally(() => {
                setisLoading(false);
            });
    }

    //const onQuoteDateChanged = (e: BaseSyntheticEvent) => { //BaseSyntheticEvent
    //    setUrl(generateUrl()) //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //    //console.log('onQuoteDateChanged(): url=', url);        
    //}
    
    return (
        <>  <h1 style={{ color: 'red'}}>Поскольку сайт bloomberg.com не разрешает CORS-запросы, для вызова API используется PROXY-сайт       
            
            <a href={props.proxy} target="_blank"> {props.proxy}</a>
            <br />
            <p>***********************************************************************************</p>
            <em>для временной разблокировки CORS Anywhere proxy требуется посещение страницы...(((</em>
            <p>***********************************************************************************</p>
        </h1>  
            <br />
            <div className="bloombergInput">
                <h1>Function component</h1>
                <label htmlFor="proxyUrl">использовать PROXY: </label>
                <input id="proxyUrl" type="text" defaultValue={props.proxy} ref={refProxy} />
                <br/><br/>
               
                <label htmlFor="quoteDate">дата котировки: </label>
                <input id="quoteDate" type="date" defaultValue={format(props.initDate, 'yyyy-MM-dd')}
                    onChange={refreshUrl} ref={refDate } />

                {/*<BloombergTimeList default={refTime} refresh={refreshUrl} />*/}
                {/*<BloombergCurrencyList  default={refCur} refresh={refreshUrl} />*/}
                <BloombergTimeList default={refTime.current} onChange={(v: string) => { refTime.current = v; refreshUrl(); } } />
                <BloombergCurrencyList default={refCur.current} onChange={(v: string) => { refCur.current = v; refreshUrl() }} />

                <br/>                

                <label htmlFor="quoteUrl">адрес API: </label>
                <input id="quoteUrl" type="text" placeholder="введите URL с любым адресом API, возвращающим JSON (например, https://catfact.ninja/fact)"
                    defaultValue={url}
                    key={url}                   
                    ref={refUrl }
                />
                <br/>
                {/* Button */ }
                <button onClick={onLoad}>Загрузить</button>
            </div>
                        
            <BloombergResponse quots={data} err={err} isLoading={isLoading} />            
    </>);
};