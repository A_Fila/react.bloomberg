import React from 'react';
import './App.css';
import { Bloomberg } from './components/BloombergLoad/Bloomberg';
import { BloombergClass } from './components/BloombergLoad/BloombergClass';
import { subDays } from 'date-fns';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import 'bootstrap/dist/css/bootstrap.min.css';

//https://react.dev/learn/sharing-state-between-components
const CORS_PROXY = 'https://cors-anywhere.herokuapp.com/'

function App() {
    return (<>
        <Tabs defaultActiveKey="profile"
            id="uncontrolled-tab-example"
            className="mb-3"
        >
            <Tab title="Function Component" eventKey="fc">
                <Bloomberg initDate={getInitDate()} proxy={CORS_PROXY} />
            </Tab>
            <Tab title="CLass Component" eventKey="cc">
                <BloombergClass initDate={getInitDate()} proxy={CORS_PROXY} />
            </Tab>

         </Tabs>
    </>
  );
}
function getInitDate(): Date {
    let initDate = new Date();
    console.log('getInitDate(), day=', initDate.getDay());
    switch (initDate.getDay()) {
        case 1://monday
            initDate = subDays(initDate, 3) // => friday
            break;
        case 0://sunday
            initDate = subDays(initDate, 2) // => friday
            break;
        default:
            initDate = subDays(initDate, 1) // => yesterday
            break;
    }
    console.log('getInitDate(), day=', initDate);
    return initDate;
}

export default App;